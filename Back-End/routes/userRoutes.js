/**
 * @Author: root
 * @Date:   2023-06-07T12:44:02+05:30
 * @Last modified by:   root
 * @Last modified time: 2023-06-08T16:55:51+05:30
 */

 const express = require("express");
 const router = express.Router();

 const {
   registerUser,
   loginUser
 } = require("../controllers/userController");

 router.post("/register", registerUser);

 router.post("/login", loginUser);

 module.exports = router;
