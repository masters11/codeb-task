/**
 * @Author: root
 * @Date:   2023-06-08T10:21:18+05:30
 * @Last modified by:   root
 * @Last modified time: 2023-06-08T13:55:50+05:30
 */


const express = require("express");
const router = express.Router();


// controllers for handling different functionalities

const {
  createTodo,
  getTodoById,
  deleteTodo,
  getAllTodos,
  updateTodo,
} = require("../controllers/todoController");

//get all todos

router.get("/todos/", getAllTodos);

//get a particular todo with given Id

router.get("/todo/:todoId", getTodoById);

//create new Todo

router.post("/todo/create", createTodo);

//update the existing Todo with the given Id

router.put("/todo/:todoId/update", updateTodo);

//delete the  todo with the given Id

router.delete("/todo/:todoId/delete", deleteTodo);

// we will export the router to import it in server.js
module.exports = router;
