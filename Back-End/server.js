/**
 * @Author: root
 * @Date:   2023-06-07T13:45:23+05:30
 * @Last modified by:   root
 * @Last modified time: 2023-06-09T15:53:11+05:30
 */

 const express = require("express");
 const app = express();
 const cors = require("cors");

 const User = require("./db/userModel");
 const Post = require("./db/todoModel");


 const userRoutes = require("./routes/userRoutes");

 const todoRoutes = require("./routes/todoRoutes");

 const {checkAuth} = require("./controllers/checkAuth");


 // require database connection
const dbConnect = require("./db/dbConnect");

// execute database connection
dbConnect();


app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use(cors());

app.use("/users", userRoutes)

app.use(checkAuth)

app.use("/api", todoRoutes);


app.listen(4000, function () {
  console.log('Example app listening on port 8000!');
 });
