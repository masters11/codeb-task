/**
 * @Author: root
 * @Date:   2023-06-09T10:18:11+05:30
 * @Last modified by:   root
 * @Last modified time: 2023-06-09T10:53:37+05:30
 */

const jwt = require("jsonwebtoken");
require("dotenv").config()

exports.checkAuth = (req,res,next) => {

  try{
    const token = req.headers.authorization.split(" ")[1];
    const verify = jwt.verify(token,process.env.TOKEN_SECRET);
    next();
  }
  catch(err){
    return res.status(401).json({
      msg:"Invalid Token",
      err
    })
  }
}
