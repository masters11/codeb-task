/**
 * @Author: root
 * @Date:   2023-06-08T11:30:42+05:30
 * @Last modified by:   root
 * @Last modified time: 2023-06-08T16:28:10+05:30
 */

const Todo = require("../db/todoModel");

exports.getAllTodos = async (req,res) => {
  const Todos = await Todo.find()
  if(!Todos){
    return res.status(201).json({
      error:"No Todos"
    })
  }
  return res.json(Todos);
}


exports.getTodoById = (req,res) => {

  if(!req.params.todoId){
    return res.status(201).json({
      error: "No ID given"
    })
  }
  Todo.findById(req.params.todoId)
    .then((todo) =>{
      if(!todo){
        return res.status(201).json({
          error: "Invalid ID"
        })
      }
      return res.json(todo);
    })
    .catch((err)=>{
      return res.status(201).json(err);
    })
}


exports.createTodo = (req,res) => {
  const todo = new Todo(req.body);

  todo.save()
    .then((todo) =>{
      if(!todo){
        return res.status(201).json({
          error: "Please fill the required value in todo"
        })
      }
      return res.status(200).json({
        message: "Todo added successfully",
        todo
      })
    })
    .catch((err) =>{
      return res.status(201).json(err);
    })
}


exports.updateTodo = async (req,res) => {
  const todoUpadated = await Todo.findById(req.params.todoId)
  if(!todoUpadated){
    return res.status(201).json({
      error: "Invalid Id"
    })
  }
  todoUpadated.todo = req.body.todo

  todoUpadated.save()
    .then((todo) =>{
      if(!todo){
        return res.status(201).json({
          error: "Problem in updating"
        })
      }
      return res.json({
        message:"Updated Successfully",
        todo
      })
    })
    .catch((err)=>{
      return res.json(err);
    })
}

exports.deleteTodo = (req,res) => {
  console.log(req.params)
  Todo.findById(req.params.todoId)
    .then((todo)=>{
      Todo.deleteOne(todo)
        .then((todo)=>{
          return res.json({
            message:"Deleted Successfully",
            todo
          })
        })
        .catch((err) =>{
          return res.json(err);
        })
    })
    .catch((err)=>{
    return res.status(201).json({
      error: "Problem finding the given ID"
    })
  })

}
