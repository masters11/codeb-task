/**
 * @Author: root
 * @Date:   2023-06-08T10:21:33+05:30
 * @Last modified by:   root
 * @Last modified time: 2023-06-08T11:44:50+05:30
 */


 const mongoose = require("mongoose");

 const TodoSchema = new mongoose.Schema(
   {
     todo: {
      type: String,
      required: [true, "Please provide a Todo!"],
      trim: true,
      maxlength: 25,
    },
  },
  {timestamps: true}
)

 module.exports =  mongoose.model("Todo", TodoSchema);
