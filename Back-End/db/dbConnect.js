/**
 * @Author: root
 * @Date:   2023-06-07T17:01:53+05:30
 * @Last modified by:   root
 * @Last modified time: 2023-06-07T17:06:43+05:30
 */
 const mongoose = require("mongoose");
 require('dotenv').config()

 async function dbConnect() {
   mongoose
   .connect(
       process.env.DB_CONNECTION,
     {
       //   these are options to ensure that the connection is done properly
       useNewUrlParser: true,
       useUnifiedTopology: true,
     }
   )
   .then(() => {
      console.log("Successfully connected to MongoDB Atlas!");
    })
    .catch((error) => {
      console.log("Unable to connect to MongoDB Atlas!");
      console.error(error);
    });
}

module.exports = dbConnect;
