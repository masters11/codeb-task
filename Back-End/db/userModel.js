/**
 * @Author: root
 * @Date:   2023-06-07T17:08:22+05:30
 * @Last modified by:   root
 * @Last modified time: 2023-06-07T17:27:58+05:30
 */

 const mongoose = require("mongoose");

 const UserSchema = new mongoose.Schema({
   email: {
    type: String,
    required: [true, "Please provide an Email!"],
    unique: [true, "Email Exist"],
  },

  password: {
    type: String,
    required: [true, "Please provide a password!"],
    unique: false,
  },
 })

 module.exports = mongoose.model.Users || mongoose.model("Users", UserSchema);
