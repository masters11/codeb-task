/**
 * @Author: root
 * @Date:   2023-06-07T10:28:41+05:30
 * @Last modified by:   root
 * @Last modified time: 2023-06-09T17:44:02+05:30
 */



import './App.css';
import Register from './pages/Register.js'
import Login from './pages/Login.js'
import Todos from "./pages/Todos.js"


function App() {
  return (
    <div>
      <Register/>
      <Login/>
      <Todos/>
    </div>
  );
}

export default App;
