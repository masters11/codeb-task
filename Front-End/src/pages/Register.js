/**
 * @Author: root
 * @Date:   2023-06-09T13:56:15+05:30
 * @Last modified by:   root
 * @Last modified time: 2023-06-10T15:14:48+05:30
 */
 /**
  * @Author: root
  * @Date:   2023-06-07T10:48:52+05:30
 * @Last modified by:   root
 * @Last modified time: 2023-06-10T15:14:48+05:30
  */
 import React, {useState} from 'react'
 import axios from "axios"

 const baseUrl = "http://localhost:4000/users"


 function Signup() {


   const [mail,setMail] = useState("")
   const [pass, setPass] = useState("")



   function postData(){
       axios.post(`${baseUrl}/login`,{
         email: mail,
         password: pass
       }).then((res) =>{
         const token = res.data.token;
         debugger;
         sessionStorage.setItem("token", token);
         console.log("perfect");
       }).catch((err)=>{
         console.log(err)
       })
   }
     return (
         <div className="container my-5">
           <div className="form-floating mb-3">
             <input type="email" className="form-control" id="floatingInput" placeholder="name@example.com" onChange = {(e) => setMail(e.target.value)}/>
             <label for="floatingInput">Email address</label>
             </div>
             <div className="form-floating">
             <input type="password" className="form-control" id="floatingPassword" placeholder="Password" onChange = {(e) => setPass(e.target.value)}/>
             <label for="floatingPassword">Password</label>
           </div>
           <button className="btn btn-primary my-3" onClick = {postData}>
             Register
           </button>
         </div>
     )
 }

 export default Signup
