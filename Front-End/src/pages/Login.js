/**
 * @Author: root
 * @Date:   2023-06-09T13:55:49+05:30
 * @Last modified by:   root
 * @Last modified time: 2023-06-09T14:00:45+05:30
 */
 /**
  * @Author: root
  * @Date:   2023-06-07T10:48:25+05:30
 * @Last modified by:   root
 * @Last modified time: 2023-06-09T14:00:45+05:30
  */


 import React from 'react'


 function Login() {
   return (
       <div className="container my-5">
         <div className="form-floating mb-3">
           <input type="email" className="form-control" id="floatingInput" placeholder="name@example.com"/>
           <label for="floatingInput">Email address</label>
           </div>
           <div className="form-floating">
           <input type="password" className="form-control" id="floatingPassword" placeholder="Password"/>
           <label for="floatingPassword">Password</label>
         </div>
         <button className="btn btn-primary my-3">
           Login
         </button>
       </div>
   )
 }

 export default Login
