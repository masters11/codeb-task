/**
 * @Author: root
 * @Date:   2023-06-09T13:58:25+05:30
 * @Last modified by:   root
 * @Last modified time: 2023-06-10T15:17:39+05:30
 */
import React,{useEffect} from 'react';
import axios from "axios";

const base = "http://localhost:4000/api";

function Todos() {

  //const todos = []



  useEffect(()=>{

    const token = sessionStorage.getItem("token")
    const config = {
  headers: { authorization: `Bearer ${token}` }
  };

    axios.get(`${base}/todos`,config)
      .then((res)=>{
        console.log(res);
      })
      .catch((err)=>{
        console.log(err)
      })
  },[])

    return (
        <div>
        yo
        </div>
    )
}

export default Todos
