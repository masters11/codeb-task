/**
 * @Author: root
 * @Date:   2023-06-07T10:28:41+05:30
 * @Last modified by:   root
 * @Last modified time: 2023-06-08T09:51:00+05:30
 */



import './App.css';
import Signup from './Components/Signup/Signup.js'
import Login from './Components/Login/Login.js'


function App() {
  return (
    <div>
      <Signup/>
      <Login/>
    </div>
  );
}

export default App;
