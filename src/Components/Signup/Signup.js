/**
 * @Author: root
 * @Date:   2023-06-07T10:48:52+05:30
 * @Last modified by:   root
 * @Last modified time: 2023-06-08T09:50:51+05:30
 */
import React from 'react'
import "./Signup.css"

function Signup() {
    return (
        <div className="container my-5">
          <div className="form-floating mb-3">
            <input type="email" className="form-control" id="floatingInput" placeholder="name@example.com"/>
            <label for="floatingInput">Email address</label>
            </div>
            <div className="form-floating">
            <input type="password" className="form-control" id="floatingPassword" placeholder="Password"/>
            <label for="floatingPassword">Password</label>
          </div>
          <button className="btn btn-primary my-3">
            Register
          </button>
        </div>
    )
}

export default Signup
